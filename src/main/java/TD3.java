/**
 *
 * @param a
 * @param b
 * @return le max entre a et b
 */
int max2(int a, int b ) {

    if ( a > b){
        return a;
    }
    return b;
}

/**
 *
 * @param a
 * @param b
 * @param c
 * @return le max entre a, b et c
 */
int max3(int a, int b , int c) {

    int max= 0;
    if ( max2(a,b) == a){
        max =max2(a,c);
    } else if ( max2(a,b) == b){
        max= max2(b, c);
    }
    return max;

}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres (int n) {

    int c = 0;
    while ( n > 0){
        n = n / 10;
        c++;
    }
    return c;
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */

int nbChiffresDuCarre(int n){

    int r = n * n;
    return nbChiffres(r);

}

/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void  repetCarac(int nb, char car){

    for ( int i = 0 ; i < nb ; i++){
        Ut.afficher(car);
    }


}

/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple (int h  , char  c ){

    int r = 0;
    int e = 1;
    int h2 = h;
    for ( int i = h ; i > 0 ; i--){
        r = h2;
        r--;
        for ( int j = 0 ; j < r ; j++ ){
            Ut.afficher(" ");
        }
        repetCarac(e,c);
        Ut.afficherSL("");
        e = e +2;
        h2--;
    }


}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissants (int nb1,int nb2 ){

    for (int i = nb1 ; i <= nb2 ; i++){System.out.print(i % 10 + " ");}
}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){

    for (int i = nb2 ; i >= nb1 ; i--){System.out.print(i % 10 + " ");}
}

/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */

void pyramideElaboree(int h) {

    for (int i = 1; i <= h; i++){
        for (int j = 0; j < h - i; j++){
            System.out.print("  ");
        }
        afficheNombresCroissants(i, i + i - 1);
        afficheNombresDecroissants(i, i + i - 2);
        System.out.println();
    }
}


/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
int racineParfaite(int c ){

    int r1 = 0;
    double r = Math.sqrt(c);
    while ( c != 0){
        if ( c -r < 0){
            return -1;
        }
        c -= r;
    }
    for ( int i = 0 ; i <= r ; i++){
         r1 = i;
    }
    return r1;
}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){

    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

/**
 *
 * @param p
 * @param q
 * @return vrai  si deux nombres entiers `p`et `q` sont amis, faux sinon.
 */
boolean nombresAmicaux(int p , int q){

    int p1 = 0;
    int q1 = 0;
    for (int i = 1; i < p ; i++) {
        if ( p % i == 0){
            p1 += i;
        }
    }
    for (int j = 1; j < q ; j++) {
        if ( q % j == 0){
            q1 += j;
        }
    }
    if ( p1 == q && q1 == p){
        System.out.println(p+" et " +q + "sont amis");
        return true;
    }
    return false;

}

/**
 * affiche à l'écran les couples de
 *nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 */

void afficheAmicaux(int max){


    for (int i = max; i > 0 ; i--) {
        for ( int j = 1 ; j <= max ; j++){
        if ( i != j && nombresAmicaux(j, i)  ) {
            System.out.println(j + " en couple avec " + i);
          }
        }
    }
}

/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2){

    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

void main (){

    Ut.afficher(racineParfaite(19));

}
